from django import forms

from tasks.models import Task


class TaskForm(forms.ModelForm):
    title = forms.CharField(max_length=30)
    description = forms.CharField(widget=forms.Textarea)
    due_date = forms.DateField(help_text="yyyy-mm-dd")

    class Meta:
        model = Task
        fields = ("title", "description", "due_date")
