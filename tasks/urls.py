from django.conf.urls import url, include
from django.urls import reverse
from tasks.views import TaskCreateView, TaskListView

app_name = "tasks"

urlpatterns = [
    url(r"^new/$", TaskCreateView.as_view(success_url="/task/list/"), name="new"),
    url(r"^list/$", TaskListView.as_view(), name="list"),
]
