from django.views.generic import ListView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from tasks.models import Task
from tasks.forms import TaskForm
from taskmanagementsite.settings import EMAIL_HOST_USER


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/task_create.html"
    fields = ["title", "description", "due_date"]
    login_url = "/users/login/"

    def form_valid(self, form):
        form.instance.user = self.request.user
        send_mail(
            form.instance.title,
            form.instance.description + " " + str(form.instance.due_date),
            EMAIL_HOST_USER,
            [EMAIL_HOST_USER],
            fail_silently=False,
        )
        return super(TaskCreateView, self).form_valid(form)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/task_list.html"
    context_object_name = "tasks"
    login_url = "/users/login/"

    def get_queryset(self):
        return Task.objects.filter(user=self.request.user)
