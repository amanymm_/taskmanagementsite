from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views

app_name = "users"

urlpatterns = [
    url(
        r"^login/$",
        auth_views.LoginView.as_view(template_name="users/login.html"),
        name="login",
    ),
    url(
        r"^logout/$",
        auth_views.LogoutView.as_view(template_name="tasks/task_list.html"),
        name="logout",
    ),
    url(r"^signup/$", views.signup, name="signup"),
    url(r"^profile/$", views.profile, name="profile"),
]
