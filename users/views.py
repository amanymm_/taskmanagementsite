from django.contrib.auth import login, authenticate, get_user
from django.shortcuts import render, redirect
from users.forms import SignUpForm
from taskmanagementsite.settings import LOGIN_REDIRECT_URL


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(LOGIN_REDIRECT_URL)
    else:
        form = SignUpForm()
    return render(request, "users/signup.html", {"form": form})


def profile(request):
    user = get_user(request)
    return render(request, "users/profile.html", {"user": user})
